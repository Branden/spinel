Spinel
========
The multi-connection multi-protocol modular bot.
- - - -    

## Configuration ##

The configuration file(s) are stored in ~/config/, the main configuration file is bot.json  

### Example Configuration for IRC ###

    {   
        "connections": [
            {
            	"name":"ExampleIRCd",
            	"type":"irc",
            	"enabled":true,
            	"realname":"ExampleBot",
            	"username":"ExampleBot",
            	"password":"",
            	"address":"irc.example.com",
            	"port":6667,
            	"channels": [
            		"#example"
            	]
            }
        ]
    }
