﻿using System.Collections.Generic;

namespace Spinel {
    
    public class Config {
        public List<ConfigConnection> connections { get; set; }
    }

    public class ConfigConnection {
        public string name { get; set; }

        public string matrix { get; set; }


        public bool enabled { get; set; }
        public string type { get; set; }

        public string address { get; set; }
        public int port { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string realname { get; set; }
        public string nspass { get; set; }
        public string operuser { get; set; }
        public string operpass { get; set; }

        public string[] channels { get; set; }
    }

}