﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Spinel {
    class Module {

        private Bot _bot;
        protected Dictionary<string, object> _modules = new Dictionary<string, object>();

        public Module(Bot bot) {
            _bot = bot;
            var modules = _getModules();

            Parallel.ForEach(modules, module => {
                Logger.info("LOADING MODULE: " + module.Name);
                Object[] args = new object[] { _bot };
                module.InvokeMember("", BindingFlags.CreateInstance, null, null, args);
                _modules.Add(module.Name, module);
            });
        }

        private List<Type> _getModules() {
            List<Type> types = new List<Type>();

            var namespaces = Assembly.GetExecutingAssembly().GetTypes().ToLookup(t => t.Namespace).ToArray();
            foreach (IGrouping<string, Type> type in namespaces) {
                if (type.Key.StartsWith("Spinel.Modules")) {
                    string className = type.Key.Split('.').Last();
                    types.Add(Type.GetType(type.Key + "." + className));
                }
            }

            return types;
        }

    }
}
