﻿using System;
using System.Net;
using Matrix.Xmpp.StreamInitiation.Profile.FileTansfer;
using Newtonsoft.Json.Linq;
using Spinel.Interfaces;

namespace Spinel.Modules.LastFm {
    class LastFm {

        public LastFm(Bot bot) {
            bot.connection.onChannelMessage += onNp;
        }

        private void onNp(IConnection connection, IPerson person, IChannel channel, string message) {
            if (!message.StartsWith("!np"))
                return;

            string[] bits = message.Split(' ');

            if (bits.Length < 2) {
                channel.message("[syntax] !np <Last.fm user>");
            } else {
                channel.message(_getSongString(bits[1]));
            }
        }

        private string _getSongString(string user) {
            var httpClient = new WebClient();
            var response = httpClient.DownloadString(String.Format("http://ws.audioscrobbler.com/2.0/?method=user.getRecentTracks&user={0}&api_key=a1bbf4327a87ce0f8c9f7116c4131932&format=json", user));
            JObject obj = JObject.Parse(response);

            if (obj["error"] != null)
                return (string)obj["message"];

            foreach (JObject track in obj["recenttracks"]["track"]) {
                if (track["@attr"] != null && track["@attr"]["nowplaying"] != null) {
                    if ((string)track["@attr"]["nowplaying"] == "true") {
                        var playCountResponse = httpClient.DownloadString(String.Format("http://ws.audioscrobbler.com/2.0/?method=track.getInfo&username={0}&mbid={1}&api_key=a1bbf4327a87ce0f8c9f7116c4131932&format=json", user, track["mbid"])); ;
                        JObject playCountObject = JObject.Parse(playCountResponse);

                        if (String.IsNullOrEmpty((string)playCountObject["error"]) && !String.IsNullOrEmpty((string) playCountObject["track"]["userplaycount"])) {
                            return String.Format("{2} is currently listening to {0} by {1}, {2} has listened to this song {3} times.", track["name"], track["artist"]["#text"], user, playCountObject["track"]["userplaycount"]);
                        } else {
                            return String.Format("{2} is currently listening to {0} by {1}", track["name"], track["artist"]["#text"], user);
                        }                        
                    }
                }

            }

            return String.Format("{0} isn't currently playing a song.", user);
        }

    }
}
