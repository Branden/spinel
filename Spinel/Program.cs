﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Newtonsoft.Json;

namespace Spinel {
    class Program {
        static void Main(string[] args) {

            var bots = new List<Bot>();

            Logger.info("Loading config.json");

            if (File.Exists(@"config/bot.json")) {
                string configFile = File.ReadAllText(@"config/bot.json");

                if (!String.IsNullOrEmpty(configFile)) {
                   var config = JsonConvert.DeserializeObject<Config>(configFile);
                    foreach (var connection in config.connections) {
                        if (!connection.enabled)
                            continue;

                        var bot = new Bot(connection);
                        new Thread(o => bot.start()).Start();
                        bots.Add(bot);
                    }
                } else {
                    Logger.critical("Failed to load config.json");
                }
            } else {
                Logger.critical("Failed to find config.json");
            }

            Console.ReadLine();
        }
    }
}
