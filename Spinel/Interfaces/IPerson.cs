﻿
namespace Spinel.Interfaces {
    public interface IPerson {

        string name { get; set; }
        string ident { get; set; }
        string host { get; set; }
        Bot bot { get; set; }


        void message(string message);

        void notice(string message);
        
    }
}
