﻿using System;

namespace Spinel.Interfaces {
    public interface IConnection {

        string host { get; set; }
        int port { get; set; }
        string username { get; set; }
        string password { get; set; }
        bool ssl { get; set; }
        Bot bot { set; get; }

        event Action<IConnection> onConnect;
        event Action<IConnection> onConnectFinish;
        event Action<IConnection> onDisconnect;
        event Action<IConnection, string> onError;

        event Action<IConnection, string> onRead;
        event Action<IConnection> onWrite;

        event Action<IConnection, string> onPing;

        event Action<IConnection> onChannelListStart;
        event Action<IConnection, IChannel> onChannelListItem;
        event Action<IConnection> onChannelListEnd;

        event Action<IConnection> onPersonListStart;
        event Action<IConnection, IPerson> onPersonListItem;
        event Action<IConnection> onPersonListEnd;

        event Action<IConnection, IPerson, IChannel> onChannelJoin;
        event Action<IConnection, IPerson, IChannel> onChannelPart;
        event Action<IConnection, IPerson, IChannel, IPerson> onChannelKick;
        event Action<IConnection, IPerson, IChannel, string> onChannelMessage;
        
        void connect();

        void disconnect();

        void write(string buffer);

    }
}
