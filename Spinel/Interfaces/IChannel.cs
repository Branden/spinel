﻿using System.Collections.Generic;

namespace Spinel.Interfaces {
    public interface IChannel {

        string name { get; set; }
        List<IPerson> people { get; set; }
        string topic { get; set; }
        Bot bot{ get; set; }


        void message(string message);

        void notice(string message);

        void join(string password = null);

        void part(string message = null);

    }
}
