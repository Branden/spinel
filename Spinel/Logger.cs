﻿using System;

namespace Spinel {
    static class Logger {

        public static void info(string message) {
            _sendToConsole("info", message);
        }

        public static void warn(string message) {
            _sendToConsole("warn", message);
        }

        public static void error(string message) {
            _sendToConsole("error", message);
        }

        public static void critical(string message) {
            _sendToConsole("critical", message);
            Environment.Exit(0);
        }

        private static void _sendToConsole(string level, string message) {
            DateTime timestamp = DateTime.Now;
            Console.WriteLine(String.Format("[{0}] [{1}] {2}", timestamp, level.ToUpper(), message));
        }

    }
}
