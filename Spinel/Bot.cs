﻿using System;
using Spinel.HipChat;
using Spinel.Interfaces;

namespace Spinel {
    public class Bot {

        private Module module;
        public ConfigConnection config;
        public IConnection connection;

        public Bot(ConfigConnection config) {
            this.config = config;
        }

        public void start() {
            initConnection();
            initModule();
            startConnection();
        }

        private void initModule() {
            Logger.info("Loading Module System");
            module = new Module(this);
        }

        private void initConnection() {
            switch (config.type) {
                case "hipchat":
                    
                    break;
                case "irc":
                    connection = new IRC.Client(this, config.address, config.port, config.username, config.password);
                    connection.onRead += debugBuffer;
                    initHooksIrc();
                    break;
            }

        }

        private void startConnection() {
            connection.connect();
        }

        private void initHooksIrc() {
            connection.onConnect += (IConnection client) => {
                client.write(String.Format("PASS {0}", client.password));
                client.write(String.Format("NICK {0}", config.username));
                client.write(String.Format("USER {0} 8 * :{1}", config.username, config.realname));
            };

            connection.onPing += (IConnection client, string server) => connection.write("PONG :"+server);

            connection.onConnectFinish += (IConnection client) => {
                client.write(String.Format("PRIVMSG NickServ :IDENTIFY {0}", config.nspass));
                client.write(String.Format("OPER {0} {1}", config.operuser, config.operpass));
                foreach (var c in config.channels) {
                    var channel = new IRC.Channel(this, c);
                    channel.join();
                } 
            };

        }

        private void debugBuffer(IConnection connection, string buffer) {
            Logger.info(buffer);
        }

    }
}
