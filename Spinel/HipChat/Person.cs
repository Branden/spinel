﻿using System;
using Matrix;
using Matrix.Xmpp;
using Matrix.Xmpp.Client;

namespace Spinel.HipChat {
    public class Person {

        public Jid jid;
        private HipChat.Client _hipchat;

        public Person(HipChat.Client hipchat, Jid iJid) {
            jid = iJid;
            _hipchat = hipchat;
        }

        public void sendMessage(string message) {
            _hipchat.send(new Message(jid, MessageType.chat, message));
        }

    }
}
