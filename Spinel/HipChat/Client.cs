﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Matrix;
using Matrix.Xmpp;
using Matrix.Xmpp.Client;
using Matrix.Xmpp.Roster;
using System.Net;

namespace Spinel.HipChat {
    public class Client {

        private string _server;
        private string _username;
        private string _password;
        public string publicName;
        private string[] _autojoin;

        private XmppClient _xmppClient;
        private MucManager _mucManager;

        private Dictionary<string, Jid> _roster = new Dictionary<string, Jid>();

        public event Action<HipChat.Client> onConnect;
        public event Action<HipChat.Client> onDisconnect;

        public event Action<HipChat.Client> onRosterStart;
        public event Action<HipChat.Client, RosterItem> onRosterItem;
        public event Action<HipChat.Client> onRosterEnd;

        public event Action<HipChat.Client, HipChat.Channel> onInvite;

        public event Action<HipChat.Client, Person, Channel, string> onHipChatMessage;
        public event Action<HipChat.Client, Person, string> onHipChatPrivateMessage;

        public Client(string server, string username, string password, string ipublicName, string[] channels = null, string licence = "") {
            _setMatrixLicense(licence);

            _server = server;
            _username = username;
            _password = password;
            publicName = ipublicName;

            _autojoin = channels;
        }

        public void start() {
            onRosterEnd += _joinAutoJoin;
            onRosterItem += _handleRosterItem;
            onInvite += _joinOnInvite;

            _init(_server, _username, _password);
        }

        private static void _setMatrixLicense(string licence) {
            Matrix.License.LicenseManager.SetLicense(licence);
        }

        private void _init(string server, string username, string password) {
            _xmppClient = new XmppClient();
            _xmppClient.SetUsername(username);
            _xmppClient.SetXmppDomain(server);
            _xmppClient.Password = password;
            _xmppClient.Show = Matrix.Xmpp.Show.chat;
            _xmppClient.Open();

            _mucManager = new MucManager(_xmppClient);

            _xmppClient.OnRosterStart += _onRosterStart;
            _xmppClient.OnRosterItem += _onRosterItem;
            _xmppClient.OnRosterEnd += _onRosterEnd;

            _mucManager.OnInvite += _onInvite;

            _xmppClient.OnMessage += _onMessage;
        }

        private void _onConnect(object sender) {
            Logger.info("HipChat Socket Connected");

            if (onConnect != null) {
                Parallel.ForEach(onConnect.GetInvocationList().ToArray(), ihook => {
                    Action<HipChat.Client> hook = (Action<HipChat.Client>)ihook;
                    hook.Invoke(this);
                });
            }
        }

        private void _onDisconnect(object sender) {
            Logger.critical("HipChat Socket Closed");

            if (onDisconnect != null) {
                Parallel.ForEach(onDisconnect.GetInvocationList().ToArray(), ihook => {
                    Action<HipChat.Client> hook = (Action<HipChat.Client>)ihook;
                    hook.Invoke(this);
                });
            }
        }

        private void _onError(object sender, Exception ex) {
            Logger.critical("HipChat Socket Error");
        }

        private void _onRosterStart(object sender, Matrix.EventArgs e) {
            if (onRosterStart != null) {
                Parallel.ForEach(onRosterStart.GetInvocationList().ToArray(), ihook => {
                    Action<HipChat.Client> hook = (Action<HipChat.Client>)ihook;
                    hook.Invoke(this);
                });
            }
        }

        private void _onRosterItem(object sender, Matrix.Xmpp.Roster.RosterEventArgs e) {
            if (onRosterItem != null) {
                Parallel.ForEach(onRosterItem.GetInvocationList().ToArray(), ihook => {
                    Action<HipChat.Client, RosterItem> hook = (Action<HipChat.Client, RosterItem>)ihook;
                    hook.Invoke(this, e.RosterItem);
                });
            }
        }

        private void _onRosterEnd(object sender, Matrix.EventArgs e) {
            if (onRosterEnd != null) {
                Parallel.ForEach(onRosterEnd.GetInvocationList().ToArray(), ihook => {
                    Action<HipChat.Client> hook = (Action<HipChat.Client>)ihook;
                    hook.Invoke(this);
                });
            }
        }

        private void _onInvite(object sender, MessageEventArgs e) {
            Channel channel = new Channel(this, _mucManager, e.Message.From);
            if (onInvite != null) {
                Parallel.ForEach(onInvite.GetInvocationList().ToArray(), ihook => {
                    Action<HipChat.Client, Channel> hook = (Action<HipChat.Client, Channel>)ihook;
                    hook.Invoke(this, channel);
                });
            }
        }

        /*
         * TODO:
         * * Clean this function...
         * * Stop using Thread.Start() and use Parallel.ForEach
         * * Handle messages that come from the API (they technically don't have a user, thus not in the roster..., why the fuck did you do this?)
         */
        private void _onMessage(object sender, MessageEventArgs msg) {
            string line = msg.Message.Body;
            string msgSender = msg.Message.From.Resource;

            if (msgSender == publicName)
                return;

            if (msg.Message.Delay != null)
                return;

            if (String.IsNullOrEmpty(line))
                return;

            HipChat.Client self = this;

            switch (msg.Message.Type) {
                case MessageType.chat:
                    Person personPriv = new Person(self, msg.Message.From);
                    if (onHipChatPrivateMessage != null) {
                        foreach (Action<HipChat.Client, Person, string> action in onHipChatPrivateMessage.GetInvocationList()) {
                            new Thread(o => action.Invoke(self, personPriv, line)).Start();
                        }
                    }
                    break;
                case MessageType.groupchat:
                    Person person = new Person(self, _roster[msgSender]);
                    Channel channel = new Channel(self, _mucManager, msg.Message.From);

                    if (onHipChatMessage != null) {
                        foreach (Action<HipChat.Client, Person, Channel, string> action in onHipChatMessage.GetInvocationList()) {
                            new Thread(o => action.Invoke(self, person, channel, line)).Start();
                        }
                    }
                    break;
            }

        }

        private void _joinOnInvite(HipChat.Client hipchat, Channel channel) {
            channel.join();
        }

        private void _joinAutoJoin(HipChat.Client hipchat) {
            foreach (string channel in _autojoin) {
                Channel chan = new Channel(hipchat, _mucManager, channel);
                chan.join();
            }
        }

        private void _handleRosterItem(HipChat.Client hipchat, RosterItem rosterItem) {
            // TODO: Figure out what the event type is, (is the user joining? quitting? etc)

            try {
                Jid jid = rosterItem.Jid;
                _roster.Add(rosterItem.Name, jid);
            } catch (Exception ex) {

            }
        }

        public void send(Message message) {
            _xmppClient.Send(message);
        }

    }
}
