﻿using System;
using System.Text;
using Matrix;
using Matrix.Xmpp;
using Matrix.Xmpp.Client;

namespace Spinel.HipChat {
    public class Channel {

        public Jid jid;
        private HipChat.Client _hipchat;
        private MucManager _mucManager;

        public Channel(HipChat.Client hipchat, MucManager mucManager, Jid iJid) {
            jid = iJid;
            _hipchat = hipchat;
            _mucManager = mucManager;
        }

        public void msg(string message) {
            Message msg = new Message(jid, MessageType.groupchat, message);

            _hipchat.send(msg);
        }

        public void join() {
            _mucManager.EnterRoom(jid, _hipchat.publicName);
        }

        public void part() {
            _mucManager.ExitRoom(jid, _hipchat.publicName);
        }

    }
}
