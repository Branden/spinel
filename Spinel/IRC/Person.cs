﻿using System;

namespace Spinel.IRC {
    class Person : Interfaces.IPerson {

        public string name { get; set; }
        public string ident { get; set; }
        public string host { get; set; }
        public Bot bot { get; set; }

        public Person(Bot bot, string name, string ident, string host) {
            this.bot = bot;
            this.name = name;
            this.ident = ident;
            this.host = host;
        }

        public void message(string message) {
            bot.connection.write(String.Format("NOTICE {0} :{1}", name, message));
        }

        public void notice(string message) {
            bot.connection.write(String.Format("NOTICE {0} :{1}", name, message));
        }

    }
}