﻿using System;
using System.Collections.Generic;
using Spinel.Interfaces;

namespace Spinel.IRC {
    class Channel : Interfaces.IChannel {

        public string name { get; set; }
        public List<IPerson> people { get; set; }
        public string topic { get; set; }
        public Bot bot { set; get; }


        public Channel(Bot bot, string name) {
            this.bot = bot;
            this.name = name;
        }

        public void message(string message) {
            bot.connection.write(String.Format("PRIVMSG {0} :{1}", name, message));
        }

        public void notice(string message) {
            bot.connection.write(String.Format("NOTICE {0} :{1}", name, message));
        }

        public void join(string password = null) {
            bot.connection.write(String.Format("JOIN {0} :{1}", name, password));
        }

        public void part(string message = null) {
            bot.connection.write(String.Format("PART {0} :{1}", name, message));
        }
    }
}
