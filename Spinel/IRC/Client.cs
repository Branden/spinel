﻿using System;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Spinel.Interfaces;

namespace Spinel.IRC {
    public class Client : IConnection {

        public string host { get; set; }
        public int port { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public bool ssl { get; set; }
        public Bot bot { get; set; }

        private Socket client { get; set; }

        public event Action<IConnection> onConnect;
        public event Action<IConnection> onConnectFinish;
        public event Action<IConnection> onDisconnect;
        public event Action<IConnection, string> onError;

        public event Action<IConnection, string> onRead;
        public event Action<IConnection> onWrite;

        public event Action<IConnection, string> onPing;

        public event Action<IConnection> onChannelListStart;
        public event Action<IConnection, IChannel> onChannelListItem;
        public event Action<IConnection> onChannelListEnd;

        public event Action<IConnection> onPersonListStart;
        public event Action<IConnection, IPerson> onPersonListItem;
        public event Action<IConnection> onPersonListEnd;

        public event Action<IConnection, IPerson, IChannel> onChannelJoin;
        public event Action<IConnection, IPerson, IChannel> onChannelPart;
        public event Action<IConnection, IPerson, IChannel, IPerson> onChannelKick;
        public event Action<IConnection, IPerson, IChannel, string> onChannelMessage;
        public event Action<IConnection, IPerson, IChannel, string> onChannelNotice;



        public Client(Bot bot, string host, int port, string username = null, string password = null) {
            this.bot = bot;
            this.host = host;
            this.port = port;
            this.username = username;
            this.password = password;
        }

        public void connect() {
            Logger.info(String.Format("Connecting to {0} on port {1}", host, port));

            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            client.BeginConnect(host, port, (IAsyncResult result) => {
                var socket = (Socket) result.AsyncState;
                socket.EndConnect(result);

                Logger.info(String.Format("Connected to {0} on port {1}", host, port));

                if (onConnect != null) {
                    Parallel.ForEach(onConnect.GetInvocationList().ToArray(), ihook => {
                        var hook = (Action<IConnection>) ihook;
                        hook.Invoke(this);
                    });
                }

                var state = new StateObject { workSocket = client };
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, read, state);

                write("BLAH"); // Let's just send that first packet because shitircds;
            }, client);

        }

        public void disconnect() {
            client.Disconnect(false);

            if (onDisconnect != null) {
                Parallel.ForEach(onDisconnect.GetInvocationList().ToArray(), ihook => {
                    var hook = (Action<IConnection>) ihook;
                    hook.Invoke(this);
                });
            }
        }

        public void read(IAsyncResult result) {
            var state = (StateObject) result.AsyncState;
            var client = state.workSocket;
            var bytesRead = client.EndReceive(result);

            var buffer = Encoding.ASCII.GetString(state.buffer, 0, bytesRead);
            var lines = Regex.Split(buffer, "(\r|\n)");

            foreach (var line in lines) {
                if (String.IsNullOrWhiteSpace(line))
                    continue;

                if (onRead != null) {
                    Parallel.ForEach(onRead.GetInvocationList().ToArray(), ihook => {
                        var hook = (Action<IConnection, string>)ihook;
                        hook.Invoke(this, line);
                    });
                }

                var person = new Person(null, null, null, null);
                var regex = new Regex("no");

                regex = new Regex(@"^:(.*?)!(.*?)@(.*?)\s.*$");
                if (regex.Match(line).Success) {
                    person = new Person(bot, (string)regex.Match(line).Groups[1].Value, (string)regex.Match(line).Groups[2].Value, (string)regex.Match(line).Groups[3].Value);
                }

                regex = new Regex(@"^PING :(.*)$");
                if (regex.Match(line).Success) {
                    if (onPing != null) {
                        Parallel.ForEach(onPing.GetInvocationList().ToArray(), (ihook) => {
                            var hook = (Action<IConnection, string>)ihook;
                            hook.Invoke(this, regex.Match(line).Groups[1].Value);
                        });
                    }
                }

                regex = new Regex(@"^.*? 376 .* :End of /MOTD command.$");
                if (regex.Match(line).Success) {
                    if (onConnectFinish != null) {
                        Parallel.ForEach(onConnectFinish.GetInvocationList().ToArray(), (ihook) => {
                            var hook = (Action<IConnection>)ihook;
                            hook.Invoke(this);
                        });
                    }
                }

                regex = new Regex(@"^:(.*?)!(.*?)@(.*?) PRIVMSG (.*?) :(.*)");
                if (regex.Match(line).Success) {
                    if (onChannelMessage != null) {
                        Parallel.ForEach(onChannelMessage.GetInvocationList().ToArray(), (ihook) => {
                            var hook = (Action<IConnection, IPerson, IChannel, string>)ihook;
                            var channel = new Channel(bot, regex.Match(line).Groups[4].Value);
                            hook.Invoke(this, person, channel, regex.Match(line).Groups[5].Value);
                        });
                    }
                }

                regex = new Regex(@"^:(.*?)!(.*?)@(.*?) NOTICE (.*?) :(.*)");
                if (regex.Match(line).Success) {
                    if (onChannelNotice != null) {
                        Parallel.ForEach(onChannelNotice.GetInvocationList().ToArray(), (ihook) => {
                            var hook = (Action<IConnection, IPerson, IChannel, string>)ihook;
                            var channel = new Channel(bot, regex.Match(line).Groups[4].Value);
                            hook.Invoke(this, person, channel, regex.Match(line).Groups[5].Value);
                        });
                    }
                }

                regex = new Regex(@"^:(.*?)!(.*?)@(.*?) JOIN (.*)");
                if (regex.Match(line).Success) {
                    if (onChannelJoin != null) {
                        Parallel.ForEach(onChannelJoin.GetInvocationList().ToArray(), (ihook) => {
                            var hook = (Action<IConnection, IPerson, IChannel>)ihook;
                            var channel = new Channel(bot, regex.Match(line).Groups[4].Value);
                            hook.Invoke(this, person, channel);
                        });
                    }
                }

                regex = new Regex(@"^:(.*?)!(.*?)@(.*?) PART (.*?) :(.*)");
                if (regex.Match(line).Success) {
                    if (onChannelPart != null) {
                        Parallel.ForEach(onChannelPart.GetInvocationList().ToArray(), (ihook) => {
                            var hook = (Action<IConnection, IPerson, IChannel>)ihook;
                            var channel = new Channel(bot, regex.Match(line).Groups[4].Value);
                            hook.Invoke(this, person, channel);
                        });
                    }
                }


            }

            client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, read, state);
        }

        public void write(string buffer) {
            var byteData = Encoding.ASCII.GetBytes(buffer + "\r\n");
            client.BeginSend(byteData, 0, byteData.Length, SocketFlags.None, (IAsyncResult result) => {
                var socket = (Socket) result.AsyncState;
                client.EndSend(result);
            }, client);
        }

    }

    public class StateObject {
        public Socket workSocket = null;
        public const int BufferSize = 1024;
        public byte[] buffer = new byte[BufferSize];
        public StringBuilder sb = new StringBuilder();
    }
}
